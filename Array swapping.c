#include<stdio.h>
#include<math.h>
int main()
{
  int a[20],n,small,big,i;
  int loc_big,loc_small,temp;
  printf("enter number of elements in array\n");
  scanf("%d",&n);
  printf("enter elements of the array\n");
  for(i=0;i<n;i++)
  {
    scanf("%d",&a[i]);
  }
  small=a[0];
  big=a[0];
  for(i=0;i<n;i++)
  {
    if(a[i]<=small)
    {
    small=a[i];
    loc_small=i;
    }
    if(a[i]>=big)
    {
      big=a[i];
      loc_big=i;
    }
  }
  printf("array before swapping\n");
  for(i=0;i<n;i++)
  {
    printf("%d\n",a[i]);
  }
  printf("largest number=%d,location=%d\n",big,loc_big);
  printf ("smallest number=%d,location=%d\n",small,loc_small);
  temp=a[loc_small];
  a[loc_small]=a[loc_big];
  a[loc_big]=temp;
  printf("After swapping\n");
  for(i=0;i<n;i++)
  {
    printf("%d\n",a[i]);
  }
  return 0;
}